# go-ResponShift
This package is created to return an API response with an internationalized message. The message response can be shifted based on the ```Accepted-Language``` stored in the Endpoint Header. This package can be used for contexts from various frameworks. such as Echo, Fiber, Mux, Gin-gonic.


## Requirement
This package requires the following package :
```go
"golang.org/x/text/language"
```

## How to use
### Import Package
From your local application, import the package with the following code

```go
import responshift "gitlab.com/sugamaulana/go-responshift"
```

Create global variable that references to go-Responshift instance in your main file
```go
    resp := Responshift{
		Lang:   language.Indonesian,
		Module: "module you want to be named",
	}
```

On handler function we must define the context 
```go
ctx := EchoContext{Context: c}
```
```c``` is reference to the framework's context. We can change the context according to the used framework
Echo :
```go
EchoContext{Context: c}
```
Fiber :
```go
FiberContext{Context: c}
```
Gin :
```go
GinContext{Context: c}
```
Mux :
```go
MuxContext{Context: c}
```
### Init Localizer
Don't forget to initialize the localizer using the following code. Init the localizer is important to load the response messages
```go
resp.InitLocalizer()
```
Beside the default messages in this package, we can add custom messages and custom languages for the responses. to make custom responses we can load the message after init localizer.
```go
cstm1Msg := []*i18n.Message{
	{
		ID:    "hello",
		Other: "hello world !",
	},
	{
		ID:    "call_info",
		Other: "this is {{.name}}",
	},
}
cstm2Msg := []*i18n.Message{
	{
		ID:    "hello",
		Other: "halo dunia !",
	},
	{
		ID:    "call_info",
		Other: "ini adalah {{.name}}",
	},
}

resp := Responshift{
	Lang:   language.Indonesian,
	Module: "localizer",
}

resp.InitLocalizer()
resp.bundle.AddMessages(language.Make("en"), cstm1Msg...)
resp.bundle.AddMessages(language.Make("id"), cstm2Msg...)
```

### Call Response Functions
After we load all messages from the bundle. Then we can call responses using the message id
Success Message :
```go
resp.OK(ctx, responshift.SuccessRead, responseData)
resp.Created(ctx, responshift.SuccessCreated, responseData)
resp.Accepted(ctx, responshift.SuccessUpdated, responseData)
```
Error Message :
```go
resp.ErrorResponse(ctx, responshift.ErrNoResult, responseData)
```

## References 
### Response
| Function                                      | Description           |
|-----------------------------------------------|-----------------------|
| ```OK(msg_id string, data interface{})```    | return the success response with status code ```200``` |
|```Created(msg_id string, data interface{})``` | return the success response with status code ```201``` |
|```Accepted(msg_id string, data interface{})```| return the success response with status code ```202``` |
|```NoContent(msg_id string, data interface{})```| return response with no content |
|```ErrorResponse(err error, data interface{})``` | return error response, such as Internal Server Error, Not Found, Bad Request, etc |

### Message ID
| Constant      | messageID         | Sample Response       |
|---------------|-------------------|-----------------------|
|SuccessInsert | ```success_create``` | ```"success insert {{.module}}"``` |
|SuccessDelete | ```success_delete``` | ```"success delete {{.module}}"``` |
|SuccessUpdate | ```success_update``` | ```"success update {{.module}}"``` |
|SuccessRead | ```success_read``` | ```"success get data {{.module}}!"``` |
|ErrNoResult | ```err_no_result``` | ```"resource {{ .module }} not found"``` |
|ErrId | ```err_invalid_id``` | ```"{{.module}} id is required"``` |
|ErrSuspended | ```err_suspended``` | ```"{{.module}} is suspended"``` |
|ErrExist | ```err_exists``` | ```"{{ .module }} already exists"``` |
|ErrDuplicateElement | ```err_duplicate_element``` | ```"duplicate key value {{.module}}"``` |
|ErrInvalidOrEmptyToken | ```err_invalid_token``` | ```"unauthorized"```|
|ErrorHttpInvalidServiceToken | ```err_invalid_service_token``` | ```"invalid service token"``` |
|ErrTokenIsExpired | ```err_token_expired``` | ```"token is expired"``` |
|ErrInvalidSignature | ```err_invalid_signature``` | ```"invalid signature key"``` |
|ErrAccountSuspended | ```err_account_suspended``` | ```"your account is suspended"``` |

## License
