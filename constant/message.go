package constant

import (
	"github.com/nicksnyder/go-i18n/v2/i18n"
	"gitlab.com/sugamaulana/go-responshift/constant/lang"
	"golang.org/x/text/language"
)

type Message struct {
	Lang language.Tag
	Msg  []*i18n.Message
}

var ListMessage []Message

func InitMessage() {

	idMessage := &Message{
		Lang: language.Indonesian,
		Msg:  lang.IdMsg,
	}
	enMessage := &Message{
		Lang: language.English,
		Msg:  lang.EnMsg,
	}
	cnMessage := &Message{
		Lang: language.Chinese,
		Msg:  lang.CnMsg,
	}
	myMessage := &Message{
		Lang: language.Malay,
		Msg:  lang.MyMsg,
	}

	ListMessage = append(ListMessage, *idMessage)
	ListMessage = append(ListMessage, *enMessage)
	ListMessage = append(ListMessage, *cnMessage)
	ListMessage = append(ListMessage, *myMessage)

}
