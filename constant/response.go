package constant

const (
	ResponseSuccess             = "success"
	ResponseCreated             = "created"
	ResponseConflict            = "conflict"
	ResponseInternalServerError = "internal_server_error"
	ResponseBadRequest          = "bad_request"
	ResponseNotFound            = "not_found"
	ResponseUnprocessableEntity = "unprocessable_entity"
	ResponseUnauthorized        = "unauthorized"
	ResponseForbidden           = "forbidden"
	ResponseInvalidParam        = "invalid_parameter_value"
	ResponseInvalidParamFormat  = "invalid_parameter_format"
	ResponseInvalidDateRange    = "invalid_date_range_parameter"
	ResponseInvalidPagination   = "invalid_pagination_parameter"
	ResponseInvalidToken        = "invalid_token"
	ResponseTokenExpired        = "token_expired"
	ResponseUserDisabled        = "user_disable"
	ResponseAccessDenied        = "access_denied"
	ResponseNoContent           = "no_content"
)
