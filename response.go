package responshift

import (
	"errors"
	"net/http"
	"time"

	"github.com/nicksnyder/go-i18n/v2/i18n"
	"gitlab.com/sugamaulana/go-responshift/constant"
	"gitlab.com/sugamaulana/go-responshift/helper"
	"gitlab.com/sugamaulana/go-responshift/schema"
	log "go.uber.org/zap"
	"golang.org/x/text/language"
)

type Responshift struct {
	Lang   language.Tag
	Module string
	bundle *i18n.Bundle
}

type Context interface {
	JSON(statusCode int, v interface{}) error
	GetHeader(key string) string
	SetCookie(cookie *http.Cookie)
}

type ParsingError struct {
	msg string
}

func (re *ParsingError) Error() string { return re.msg }

// ================RESPONSE 2XX======================
// Status Code : 200
func (r *Responshift) OK(ctx Context, message string, data interface{}) error {
	log.S().Info("success response")

	return ctx.JSON(http.StatusOK, schema.Base{
		Status:     constant.ResponseSuccess,
		StatusCode: http.StatusOK,
		Message:    getLocalizedString(ctx, r.bundle, message, r.Module),
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	})
}

// Status Code : 201
func (r *Responshift) Created(ctx Context, message string, data interface{}) error {
	log.S().Info("created response")

	return ctx.JSON(http.StatusCreated, schema.Base{
		Status:     constant.ResponseCreated,
		StatusCode: http.StatusCreated,
		Message:    getLocalizedString(ctx, r.bundle, message, r.Module),
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	})
}

// Status Code : 202
func (r *Responshift) Accepted(ctx Context, message string, data interface{}) error {
	log.S().Info("accepted response")

	return ctx.JSON(http.StatusAccepted, schema.Base{
		Status:     constant.ResponseSuccess,
		StatusCode: http.StatusAccepted,
		Message:    getLocalizedString(ctx, r.bundle, message, r.Module),
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	})
}

// Status Code : 204
func (r *Responshift) NoContent(ctx Context, message string) error {
	log.S().Info("no content response")

	return ctx.JSON(http.StatusNoContent, schema.Base{
		Status:     constant.ResponseNoContent,
		StatusCode: http.StatusNoContent,
		Message:    getLocalizedString(ctx, r.bundle, message, r.Module),
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
	})
}

// ErrorResponse returns
func (r *Responshift) ErrorResponse(ctx Context, err error, data interface{}) error {
	statusCode, errCode := helper.ErrorType(err)
	switch statusCode {
	case http.StatusConflict:
		return r.Conflict(ctx, errCode, data)
	case http.StatusBadRequest:
		return r.BadRequest(ctx, errCode, data)
	case http.StatusNotFound:
		return r.NotFound(ctx, errCode, data)
	case http.StatusUnauthorized:
		return r.Unauthorized(ctx, errCode, data)
	case http.StatusForbidden:
		return r.Forbidden(ctx, errCode, data)
	}
	return r.InternalServerError(ctx, errCode, data)
}

// ====================RESPONSE 4XX=======================
// Status Code : 400
func (r *Responshift) BadRequest(ctx Context, errCode string, data interface{}) error {
	msg := getLocalizedString(ctx, r.bundle, errCode, r.Module)

	responseData := schema.Base{
		Status:     constant.ResponseBadRequest,
		StatusCode: http.StatusBadRequest,
		Message:    msg,
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	}

	log.S().Errorf("bad request error : %s ", msg)

	return ctx.JSON(http.StatusBadRequest, responseData)
}

// Status Code : 401
func (r *Responshift) Unauthorized(ctx Context, errCode string, data interface{}) error {
	msg := getLocalizedString(ctx, r.bundle, errCode, r.Module)
	responseData := schema.Base{
		Status:     constant.ResponseUnauthorized,
		StatusCode: http.StatusUnauthorized,
		Message:    msg,
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	}

	log.S().Errorf("unauthorized error : %s ", msg)

	return ctx.JSON(http.StatusUnauthorized, responseData)
}

// Status Code : 403
func (r *Responshift) Forbidden(ctx Context, errCode string, data interface{}) error {
	msg := getLocalizedString(ctx, r.bundle, errCode, r.Module)
	responseData := schema.Base{
		Status:     constant.ResponseForbidden,
		StatusCode: http.StatusForbidden,
		Message:    msg,
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	}

	log.S().Errorf("forbidden error : %s ", msg)

	return ctx.JSON(http.StatusForbidden, responseData)
}

// Status Code : 404
func (r *Responshift) NotFound(ctx Context, errCode string, data interface{}) error {
	msg := getLocalizedString(ctx, r.bundle, errCode, r.Module)

	responseData := schema.Base{
		Status:     constant.ResponseNotFound,
		StatusCode: http.StatusNotFound,
		Message:    msg,
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	}

	log.S().Errorf("error not found : %s ", msg)

	return ctx.JSON(http.StatusNotFound, responseData)
}

// Status Code : 409
func (r *Responshift) Conflict(ctx Context, errCode string, data interface{}) error {
	msg := getLocalizedString(ctx, r.bundle, errCode, r.Module)

	responseData := schema.Base{
		Status:     constant.ResponseConflict,
		StatusCode: http.StatusConflict,
		Message:    msg,
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	}

	log.S().Errorf("conflict data error : %s ", msg)

	return ctx.JSON(http.StatusConflict, responseData)
}

// Status Code 422
func (r *Responshift) ErrorParsing(ctx Context, errCode string, data interface{}) error {

	msg := getLocalizedString(ctx, r.bundle, errCode, r.Module)

	responseData := schema.Base{
		Status:     constant.ResponseUnprocessableEntity,
		StatusCode: http.StatusUnprocessableEntity,
		Message:    msg,
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	}

	log.S().Errorf("parsing data error : %s ", msg)

	return ctx.JSON(http.StatusUnprocessableEntity, responseData)
}

// ===========================RESPONSE 5XX===========================
// Status Code 500
func (r *Responshift) InternalServerError(ctx Context, errCode string, data interface{}) error {
	msg := getLocalizedString(ctx, r.bundle, errCode, r.Module)

	responseData := schema.Base{
		Status:     constant.ResponseInternalServerError,
		StatusCode: http.StatusInternalServerError,
		Message:    msg,
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	}

	log.S().Errorf("internal server error : %s ", msg)

	return ctx.JSON(http.StatusInternalServerError, responseData)
}

// ErrorValidate returns
func (r *Responshift) ErrorValidate(ctx Context, errCode string, data interface{}) error {
	message := helper.SwitchErrorValidation(errors.New(errCode))
	responseData := schema.Base{
		Status:     constant.ResponseBadRequest,
		StatusCode: http.StatusBadRequest,
		Message:    message,
		Timestamp:  time.Now().UTC().Format("2006-01-02T15:04:05.00Z"),
		Data:       data,
	}

	log.S().Errorf("validate data error : %s ", errCode)

	return ctx.JSON(http.StatusBadRequest, responseData)
}

// SetCookie returns
func SetCookie(ctx Context, data map[string]string) {
	for key, value := range data {
		cookie := &http.Cookie{}
		cookie.Name = key
		cookie.Value = value
		cookie.HttpOnly = true
		cookie.Path = "/"
		ctx.SetCookie(cookie)
	}
}
