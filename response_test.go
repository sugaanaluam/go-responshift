package responshift

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"golang.org/x/text/language"
)

func TestEchoResponseSuccess(t *testing.T) {
	// Set up Echo context
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set("Accept-Language", "zh")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	resp := Responshift{
		Lang:   language.Indonesian,
		Module: "image-api",
	}
	resp.InitLocalizer()
	ctx := EchoContext{Context: c}
	// Call JSONSuccess function
	err := resp.ErrorResponse(ctx, errors.New("err_no_result"), "success_data")
	fmt.Println(rec.Body.String())

	// Assertions
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusNotFound, rec.Code)
		assert.JSONEq(t, fmt.Sprintf(`{"status":"not_found", "statusCode":404,"message":"%s", "timestamp":"%s","data":"success_data"}`, getLocalizedString(ctx, resp.bundle, "err_no_result", resp.Module), time.Now().UTC().Format("2006-01-02T15:04:05.00Z")), rec.Body.String())
	}
}

func TestGinResponseSuccess(t *testing.T) {
	gin.SetMode(gin.TestMode)
	w := httptest.NewRecorder()
	ctx, _ := gin.CreateTestContext(w)
	ctx.Request = &http.Request{
		Header: make(http.Header),
		URL:    &url.URL{},
	}
	ctx.Request.Header.Set("Content-Type", "application/json")
	ctx.Request.Header.Set("Accept-Language", "zh")

	resp := Responshift{
		Lang:   language.Chinese,
		Module: "image-api",
	}
	resp.InitLocalizer()
	gctx := GinContext{Context: ctx}
	// Call JSONSuccess function
	err := resp.OK(gctx, "success_read", "success_data")
	fmt.Println(w.Body.String())

	// Assertions
	if assert.NoError(t, err) {
		assert.Equal(t, http.StatusOK, w.Code)
		assert.JSONEq(t, fmt.Sprintf(`{"status":"success", "statusCode":200,"message":"%s", "timestamp":"%s","data":"success_data"}`, getLocalizedString(gctx, resp.bundle, "success_read", resp.Module), time.Now().UTC().Format("2006-01-02T15:04:05.00Z")), w.Body.String())
	}
}

// func TestFiberResponseSuccess(t *testing.T) {
// 	app := fiber.New()
// 	var ctx FiberContext
// 	reshift := Responshift{
// 		Lang:   language.Chinese,
// 		Module: "image-api",
// 	}
// 	reshift.InitLocalizer()
// 	// Define a test route that calls JSONSuccess
// 	app.Get("/test", func(c *fiber.Ctx) error {
// 		ctx.Context = c

// 		// Call JSONSuccess function
// 		return reshift.OK(ctx, "success_read", "success_data")
// 	})

// 	// Create a test request
// 	req := httptest.NewRequest("GET", "/test", nil)
// 	resp, err := app.Test(req)
// 	b, _ := io.ReadAll(resp.Body)

// 	// Assertions
// 	if assert.NoError(t, err) {
// 		assert.Equal(t, http.StatusOK, resp.StatusCode)
// 		assert.JSONEq(t, fmt.Sprintf(`{"status":"success", "statusCode":200,"message":"%s", "timestamp":"%s","data":"success_data"}`, getLocalizedString(ctx, reshift.bundle, "success_read", reshift.Module), time.Now().UTC().Format("2006-01-02T15:04:05.00Z")), string(b))
// 	}
// }
